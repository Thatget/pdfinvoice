<?php

namespace Pawan\PdfCustomiser\Model\Sales\Pdf;

class Totaldue extends \Magento\Sales\Model\Order\Pdf\Total\DefaultTotal{

    protected $_taxConfig;

    public function __construct(
        \Magento\Tax\Model\Config $config,
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Tax\Model\Calculation $taxCalculation,
        \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory $ordersFactory,
        array $data = []
    )
    {
        $this->_taxConfig = $config;
        parent::__construct($taxHelper, $taxCalculation, $ordersFactory, $data);
    }

    public function getTotalsForDisplay()
    {
        $store = $this->getOrder()->getStore();
        if ($this->_taxConfig->displaySalesTaxWithGrandTotal($store)){
            return parent::getTotalsForDisplay();
        }

        $totalpaid = $this->getOrder()->formatPriceTxt($this->getSource()->getTotalPaid());

        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;

        $totals = [
            [
                'amount' => $this->getAmountPrefix() . $totalpaid,
                'label' => __('Total Paid') . ':',
                'font_size' => $fontSize,
            ],
        ];

        $totals[] = [
            'amount' => $this->getAmountPrefix() . $totalpaid,
            'label' => __('Total Paid') . ':',
            'font_size' => $fontSize,
        ];
        return $totals;
    }

}