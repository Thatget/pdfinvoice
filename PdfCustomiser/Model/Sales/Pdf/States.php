<?php
namespace Pawan\PdfCustomiser\Model\Sales\Pdf;

class States extends \Magento\Sales\Model\Order\Pdf\Total\DefaultTotal{

    public function __construct(
        \Magento\Tax\Helper\Data $taxHelper,
        \Magento\Tax\Model\Calculation $taxCalculation,
        \Magento\Tax\Model\ResourceModel\Sales\Order\Tax\CollectionFactory $ordersFactory,
        array $data = []
    )
    {
        parent::__construct($taxHelper, $taxCalculation, $ordersFactory, $data);
    }

    public function getTotalsForDisplay()
    {
        $store = $this->getOrder()->getState();

        $totals = [
            [
                'amount' => $this->getOrder()->getState(),
                'label' => __('State') . ':',
                'font_size' => 7,
            ],
        ];

        return parent::getTotalsForDisplay();
    }
}